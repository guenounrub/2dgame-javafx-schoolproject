import java.io.IOException;

import javafx.animation.PauseTransition;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public class MainApp extends Application {

    @Override
    public void start( Stage primaryStage) throws IOException, InterruptedException {
        /* Mettre ici le chemin vers la page d'accueil du jeux*/
		primaryStage.initStyle(StageStyle.UNDECORATED);

		Parent Demarrage = FXMLLoader.load(getClass().getResource("Menu/fxml/Demarrage.fxml"));
		Scene scene0 = new Scene(Demarrage);
		primaryStage.setScene(scene0);
		primaryStage.show();
		Parent root = FXMLLoader.load(getClass().getResource("Menu/fxml/Menu.fxml"));
		Scene scene = new Scene(root);

		final PauseTransition pause = new PauseTransition(Duration.millis(5000));
		pause.setOnFinished(event -> {

		primaryStage.setScene(scene);
		primaryStage.show();

		});
		// Lancer l'animation.
		pause.play();
    }
    public static void main( String[] args) throws IOException {
        launch(args);
    }
}