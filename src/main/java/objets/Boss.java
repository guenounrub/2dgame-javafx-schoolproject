package objets;

import room.Salle;

public class Boss extends Ennemi{
	// Images des enemmis
	private static String IMGHAUT = "room/imgs/skinEnnemis/Boss/up.png";
	private static String IMGBAS = "room/imgs/skinEnnemis/Boss/down.png";
	private static String IMGGAUCHE = "room/imgs/skinEnnemis/Boss/left.png";
	private static String IMGDROITE = "room/imgs/skinEnnemis/Boss/right.png";

	public Boss(double x, double y, String direction) {
		super(x, y, 200, 100, getImage(direction));
	}

	public String toString(){
		return "Boss : x = " + (int)this.getX() + ", y = " + (int)this.getY() + " / Vie = " + this.getVie() + ", Atk = " + this.getAtk();
	}

			/**
	 * Fonction qui choisit l'image de l'ennemi en fonction de la direction souhaité
	 * @param direction
	 * @return Le chemin de la bonne image
	 */
	private static String getImage(String direction) {
		String img = "";
		switch (direction) {
			case HAUT:
				img = IMGHAUT;
				break;
			case BAS:
				img = IMGBAS;
				break;
			case DROITE:
				img = IMGDROITE;
				break;
			case GAUCHE:
				img = IMGGAUCHE;
				break;
		}
		return img;
	}

	// Fct du pattern composite
	public void combattre(Joueur j,Salle s){
		if(j.getAtk() > this.getVie()) {
			s.rmEnnemis(this);
			afficherMenuFin(s);
		}
		else {
			j.setVie(j.getVie()-(this.getAtk()-j.getAtk()));
			this.setVie(this.getVie()-j.getAtk());
		}
		if((this.getVie() <= 0 ) || (j.getVie() <= 0)) { // Si le boss est mort
			afficherMenuFin(s);
		}
	}

	public void update(){
		this.setVie(this.getVie()+100);
		this.setAtk(this.getAtk()+40);
	}

}