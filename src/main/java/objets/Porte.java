package objets;

import javafx.scene.Group;
import javafx.scene.input.KeyCode;
import javafx.scene.robot.Robot;
import room.Salle;



public class Porte extends ItemConsommable{
	private final static String OPENIMG = "room/imgs/skinsItems/porte3.png";
	private final static String CLOSEIMG = "room/imgs/skinsItems/porte1.png";
	
	private Salle destination;
	private boolean isOpen;
	
	public Porte(double x, double y, Salle destination, boolean isOpen) {
		super(x,y,CLOSEIMG);
		this.isOpen = isOpen;
		this.destination = destination;
	}

	public Porte(double x, double y, Salle destination){
		this(x,y,destination,false);
	}

	public Porte(){
		this(0,0,null,false);
	}

	public Porte(boolean isOpen){
		this(0,0,null,isOpen);
	}

	public Salle getDestination(){
		return this.destination;
	}

	public boolean getIsOpen(){
		return this.isOpen;
	}
	public String toString(){
		return "Porte : x = " + (int)this.getX() + ", y = " + (int)this.getY() + ", Etat : " + ((this.isOpen) ? "ouverte" : "fermée");
	}
	
	public void changerScene(){
		Group g = (Group)this.getItem().getScene().getRoot();
		g.getScene().setRoot(this.destination.getPlateau());
	}

	public void ouvrirPorte() throws InterruptedException {
		Robot r = new Robot();
		r.keyPress(KeyCode.LEFT);
		r.keyRelease(KeyCode.LEFT);
		r.keyPress(KeyCode.LEFT);
		r.keyRelease(KeyCode.LEFT);
		r.keyPress(KeyCode.LEFT);
		r.keyRelease(KeyCode.LEFT);
		r.keyPress(KeyCode.RIGHT);
		r.keyRelease(KeyCode.RIGHT);
		Thread.sleep(500);
		if(!this.isOpen){
			this.setImage(OPENIMG);
			this.isOpen = true;
			this.changerScene();
		}else{
			this.setImage(CLOSEIMG);
			this.isOpen = false;
		}
		this.getItem().relocate(this.getX(),this.getY());
	}

	public void ramasser(Joueur j, Salle s) {
		try {
			this.ouvrirPorte();
			if (this.getIsOpen()) {
				// this.setCoordoneePorte(j, s);
				j.setX(96);
				j.setY(96);
				s.rmJoueur(j);
				this.getDestination().setJoueur(j);
				j.getItem().relocate(j.getX(), j.getY());
			}		
		} catch (InterruptedException e) {
			System.err.println("Erreur Exception useItem");
		}
	}
}