package objets;

import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public abstract class Item {
	private String path;	
	private double x;
	private double y;

	private Image itemImage;
	private Node  item;

	public Item(){
		this.x = 0;
		this.y = 0;
		this.path = "";
		this.itemImage = null;
		this.item = null;
	}

	public Item(double x,double y,String path) {
		this.x = x;
		this.y = y;
		this.path = path;
		this.itemImage = new Image(path);
		this.item = new ImageView(this.itemImage);
		this.item.relocate(x,y);
	}

	public double getX() {
		return this.x;
	}

	public Image getItemImage(){
		return this.itemImage;
	}

	public Node getItem(){
		return this.item;
	}

	public double getY() {
		return this.y;
	}
	
	public String getPath(){
		return this.path;
	}

	public void setX(double x){
		this.x = x;
	}
	public void setY(double y){
		this.y = y;
	}

	public void setImage(String path){
		Group g = (Group)this.item.getScene().getRoot();
		g.getChildren().remove(this.item);
		this.itemImage = new Image(path);
		this.item = new ImageView(this.itemImage);
		g.getChildren().add(this.item);		
	}

	// public String toString(){
	// 	return "Item : X= "+this.getX()+" ; Y = "+this.getY();
	// }
}
