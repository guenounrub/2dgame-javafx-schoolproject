package objets;

import java.util.ArrayList;

import org.javatuples.Pair;
import org.javatuples.Triplet;

import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import room.Salle;


public class Joueur extends ItemNonConsommable {

	//Patttern Singleton
	private static Joueur instance;

	// Distance de déplacement élémentaire en X et Y
	private static double dx = 10;
	private static double dy = 10;

	private int vie;
	private int atk;

	private final static String HAUT = "UP", BAS = "DOWN", GAUCHE = "LEFT", DROITE = "RIGHT";
	private String direction;

	// sert à faire défiler les images
	private static int i = 1;
	private static boolean smooth = true;

	private Stage menu;

	public Joueur() {
		super(96, 96, "room/imgs/skinJoueur/right1.png");
		this.vie = 100;
		this.atk = 40;
		this.direction = DROITE;
	}

	public int getVie() {
		return this.vie;
	}

	public int getAtk() {
		return this.atk;
	}

	public String getDirection() {
		return this.direction;
	}

	public void setVie(int vie) {
		this.vie = vie;
	}

	public void setAtk(int atk) {
		this.atk = atk;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public String toString() {
		return "Vous : x = " + (int)this.getX() + ", y = " + (int)this.getY() + " / Vie = " + this.getVie() + ", Atk = " + this.getAtk();
	}

	public static Joueur getInstance(){
		if(instance != null){
			return instance;
		}
		synchronized (Joueur.class){
			if(instance == null){
				instance = new Joueur();
			}
			return instance;
		}
	}

	/** Règles de déplacement */

	/**
	 * Fonction qui modifie les images pour générer l'animation de déplacement du
	 * joueur
	 * 
	 * @param direction Direction de déplacement
	 * @return L'image suivante de l'animation a charger
	 */
	public String updateImage(String direction) {
		String newPath = "room/imgs/skinJoueur/";
		// Si le joueur change de direction de marche alors on le modifie
		if (this.direction != direction) {
			this.setDirection(direction);
		}
		switch (direction) {
			// On change les coordonées de l'image en prenant en compte le déplacement
			case HAUT:
				newPath += "up";
				break;
			case BAS:
				newPath += "down";
				break;
			case GAUCHE:
				newPath += "left";
				break;
			case DROITE:
				newPath += "right";
				break;
		}
		// On choisit la bonne image pour l'effet de marche
		if (i == 1) {
			if (smooth) {
				i = 2;
			} else {
				i = 0;
			}
		} else {
			i = 1;
			if (smooth) {
				smooth = false;
			} else {
				smooth = true;
			}
		}
		newPath += i + ".png";
		return newPath;
	}

	/**
	 * Fonction qui gère les déplacements du joueur
	 * @param s Salle du joueur
	 * @param direction Direction que le joueur prend
	 */
	public void move(String direction,Salle s) {
		switch (direction) {
			// On change les coordonées de l'image en prenant en compte le déplacement
			case HAUT:
				// System.out.println("Collision haut ? "+this.isColision(HAUT, s));
				if (!this.isColision(HAUT, s))
					this.setY(this.getY() - dy);
				break;
			case BAS:
				if (!this.isColision(BAS, s))
					this.setY(this.getY() + dy);
				break;
			case GAUCHE:
				if (!this.isColision(GAUCHE, s))
					this.setX(this.getX() - dx);
				break;
			case DROITE:
				if (!this.isColision(DROITE, s))
					this.setX(this.getX() + dx);
				break;
		}
		// On change l'image
		this.setImage(updateImage(direction));
		// On déplace l'image
		this.getItem().relocate(this.getX(), this.getY());
	}

	/* Règles de colisions */

	/**
	 * Fonction qui renvoie vrai si entre V1 et V2 il y a une écart d'au moins ecart
	 * 
	 * @param v1    Première valeur
	 * @param v2    Deuxième valeur
	 * @param ecart Ecart souhaité entre les valeurs
	 * @return True si v1 - v2 > ecart false sinon
	 */
	private static boolean isEcart(double v1, double v2, double ecart) {
		return Math.abs(v1 - v2) > ecart;
	}

	/**
	 * Fonction qui détecte s'il y a une collision lors du déplacement vers le haut
	 */
	public boolean isCollisionUpItem(Item item){
		boolean collision = false;
		if (this.getY() > item.getY() && !isEcart(this.getY(), item.getY(), 24) && !isEcart(item.getX(), this.getX(), 19)){
			collision = true;
			// System.out.println("Collison Up : "+this.toString() +"avec" + item);
		}
		return collision;
	}

		/**
	 * Fonction qui détecte s'il y a une collision lors du déplacement vers le haut
	 */
	public boolean isCollisionDownItem(Item item){
		boolean collision = false;
		if (this.getY() < item.getY() && !isEcart(item.getY(), this.getY(), 39) && !isEcart(this.getX(), item.getX(), 19))
			collision = true;
		return collision;
	}

		/**
	 * Fonction qui détecte s'il y a une collision lors du déplacement vers le haut
	 */
	public boolean isCollisionLeftItem(Item item){
		boolean collision = false;
		if (this.getX() > item.getX() &&!isEcart(this.getX(), item.getX(), 32) && !isEcart(item.getY(), this.getY(), 19))
			collision = true;
		return collision;
	}

		/**
	 * Fonction qui détecte s'il y a une collision lors du déplacement vers le haut
	 */
	public boolean isCollisionRightItem(Item item){
		boolean collision = false;
		if (this.getX() < item.getX() && !isEcart(item.getX(), this.getX(), 32) && !isEcart(this.getY(), item.getY(), 19))
			collision = true;
		return collision;
	}


	/**
	 * Fonction qui détecte s'il y a une collision lors du déplacement vers le haut
	 * 
	 * @param p Tuple avec la liste des objets et des ennemis
	 * @return True si le personnage va rentrer en collision en se déplacement vers le haut
	 * @return False sinon
	 */
	public boolean isCollisionUp(Triplet<ArrayList<ItemConsommable>,ArrayList<Ennemi>,ArrayList<ItemDesign>> t) {
		boolean collision = false;
		ArrayList<ItemConsommable> objets = t.getValue0();
		ArrayList<Ennemi> ennemis = t.getValue1();
		ArrayList<ItemDesign> deco = t.getValue2();
		for (int i = 0; i < objets.size(); i++) {
			ItemConsommable item = objets.get(i);
			collision = collision || this.isCollisionUpItem(item);
		}
		for (int i = 0; i < ennemis.size(); i++) {
			Ennemi en = ennemis.get(i);
			collision = collision || this.isCollisionUpItem(en);
		}
		for (int i = 0; i < deco.size(); i++) {
			ItemDesign it = deco.get(i);
			if (!(it instanceof Tile))
				collision = collision || this.isCollisionUpItem(it);
		}
		return collision;
	}

	/**
	 * Fonction qui détecte si il y a une collision lors du déplacement vers le bas
	 *
	 * @param items Liste des items présent sur le plateau
	 * @return True si le personnage va rentrer en collision en se déplacement vers le bas
	 * @return False sinon
	 */
	public boolean isCollisionDown(Triplet<ArrayList<ItemConsommable>,ArrayList<Ennemi>,ArrayList<ItemDesign>> t) {
		boolean collision = false;
		ArrayList<ItemConsommable> objets = t.getValue0();
		ArrayList<Ennemi> ennemis = t.getValue1();
		ArrayList<ItemDesign> deco = t.getValue2();
		for (int i = 0; i < objets.size(); i++) {
			ItemConsommable item = objets.get(i);
			collision = collision || this.isCollisionDownItem(item);
			
		}
		for (int i = 0; i < ennemis.size(); i++) {
			Ennemi en = ennemis.get(i);
			collision = collision || this.isCollisionDownItem(en);
		}
		for (int i = 0; i < deco.size(); i++) {
			ItemDesign it = deco.get(i);
			if (!(it instanceof Tile))
				collision = collision || this.isCollisionDownItem(it);
		}
		return collision;
	}

	/**
	 * Fonction qui détecte si il y a une collision lors du déplacement vers la droite
	 *
	 * @param items Liste des items présent sur le plateau
	 * @return True si le personnage va rentrer en collision en se déplacement vers la droite
	 * @return False sinon
	 */
	public boolean isCollisionRight(Triplet<ArrayList<ItemConsommable>,ArrayList<Ennemi>,ArrayList<ItemDesign>> t) {
		boolean collision = false;
		ArrayList<ItemConsommable> objets = t.getValue0();
		ArrayList<Ennemi> ennemis = t.getValue1();
		ArrayList<ItemDesign> deco = t.getValue2();
		for (int i = 0; i < objets.size(); i++) {
			ItemConsommable item = objets.get(i);
			collision = collision || this.isCollisionRightItem(item);
		}
		for (int i = 0; i < ennemis.size(); i++) {
			Ennemi en = ennemis.get(i);
			collision = collision || this.isCollisionRightItem(en);
		}
		for (int i = 0; i < deco.size(); i++) {
			ItemDesign it = deco.get(i);
			if (!(it instanceof Tile))
				collision = collision || this.isCollisionRightItem(it);
		}
		return collision;
	}

	/**
	 * Fonction qui détecte si il y a une collision lors du déplacement vers la gauche
	 *
	 * @param items Liste des items présent sur le plateau
	 * @return True si le personnage va rentrer en collision en se déplacement vers la gauche
	 * @return False sinon
	 */
	public boolean isCollisionLeft(Triplet<ArrayList<ItemConsommable>,ArrayList<Ennemi>,ArrayList<ItemDesign>> t) {
		boolean collision = false;
		ArrayList<ItemConsommable> objets = t.getValue0();
		ArrayList<Ennemi> ennemis = t.getValue1();
		ArrayList<ItemDesign> deco = t.getValue2();
		for (int i = 0; i < objets.size(); i++) {
			ItemConsommable item = objets.get(i);
			collision = collision || this.isCollisionLeftItem(item);
		}
		for (int i = 0; i < ennemis.size(); i++) {
			Ennemi en = ennemis.get(i);
			collision = collision || this.isCollisionLeftItem(en);
		}
		for (int i = 0; i < deco.size(); i++) {
			ItemDesign it = deco.get(i);
			if (!(it instanceof Tile))
				collision = collision || this.isCollisionLeftItem(it);
		}
		return collision;
	}

	/**
	 * Fonction qui détermine si il y a une collision en se déplaçant dans la direction passée en paramètre
	 * 
	 * @param direction Direction de déplacement du joueur
	 * @return True si colision False sinon
	 */
	public boolean isColision(String direction, Salle s) {
		boolean collision = false;
		Triplet<ArrayList<ItemConsommable>,ArrayList<Ennemi>,ArrayList<ItemDesign>> tmp = s.getObjetsCollisions();
		switch (direction) {
			case HAUT:
				collision = this.isCollisionUp(tmp);
				break;
			case BAS:
				collision = this.isCollisionDown(tmp);
				break;
			case GAUCHE:
				collision = this.isCollisionLeft(tmp);
				break;
			case DROITE:
				collision = this.isCollisionRight(tmp);
				break;
		}
		return collision;
	}

	/* Fonction qui détermine si le joueur et a coté d'un item */

	/**
	 * Fonction qui détermine si l'item est à droite du joueur
	 * @param i Item
	 * @return True si l'item et le joueur sont a une distance de maximum 20 pixels
	 *         False sinon
	 */
	public boolean isVoisinRight(Item i) {
		if (isEcart(i.getX(), this.getX(), 50)) { // on regarde si litem est proche du joueur
			return false;
		} else {
			if (isEcart(i.getY(), this.getY(), 39) || isEcart(this.getY(), i.getY(), 39)) // On regarde si le joueur est à la même hauteur que l'item
				return false;
			else
				return true;
		}
	}


	/**
	 * Fonction qui détermine si l'item est à gauche du joueur
	 * @param i Item
	 * @return True si l'item et le joueur sont a une distance de maximum 20 pixels
	 *         False sinon
	 */
	public boolean isVoisinLeft(Item i) {
		if (isEcart(this.getX(), i.getX(), 50)) // on regarde si l'item est proche du joueur
			return false;
		else {
			if (isEcart(i.getY(), this.getY(), 39) || isEcart(this.getY(), i.getY(), 39)) // On regarde si le joueur est à la même hauteur que l'item
				return false;
			else
				return true;
		}
	}

	/**
	 * Fonction qui détermine si l'item est au dessus du joueur
	 *
	 * @param i Item
	 * @return True si l'item et le joueur sont a une distance de maximum 20 pixels
	 *         False sinon On reprend la fonction des colision car c'est le même
	 *         principe
	 */
	public boolean isVoisinUp(Item i) {
		return (this.getY() > i.getY() && !isEcart(this.getY(), i.getY(), 39) && !isEcart(this.getX(), i.getX(), 19));
	}

	/**
	 * Fonction qui détecte si il y a une colision lors du déplacement vers le bas
	 * @param items Liste des items présent sur le plateau
	 * @return True si le personnage va rentrer encolision en se déplacement vers le
	 *         bas
	 * @return False sinon
	 */
	public boolean isVoisinDown(Item i) {
		return (this.getY() < i.getY() && !isEcart(i.getY(), this.getY(), 39) && !isEcart(this.getX(), i.getX(), 19));
	}

	/**
	 * Fonction qui détermine si le joueur est voisin de l'item i
	 * 
	 * @param i Item
	 * @return True si l'item et le joueur sont a une distance de maximum 20 pixels
	 *         False sinon
	 */
	public boolean isVoisinItem(Item i) {
		boolean isVoisin = false;
		switch (this.direction) {
			case HAUT:
				isVoisin = this.isVoisinUp(i);
				break;
			case BAS:
				isVoisin = this.isVoisinDown(i);
				break;
			case GAUCHE:
				isVoisin = this.isVoisinLeft(i);
				break;
			case DROITE:
				isVoisin = this.isVoisinRight(i);
				break;
		}
		return isVoisin;
	}

	/**
	 * Fonction qui regarde si des items sont utilisés
	 * @param s Salle des items
	 */
	public void useItem(Salle s) {
		Pair<ArrayList<ItemConsommable>,ArrayList<Ennemi>> items = new Pair<ArrayList<ItemConsommable>,ArrayList<Ennemi>> (s.getObjetsCollisions().getValue0(),s.getObjetsCollisions().getValue1());
		for (int i = 0; i < items.getValue0().size(); i++) {
			ItemConsommable it = items.getValue0().get(i);
			if(this.isVoisinItem(it)){
				it.ramasser(this,s);
			}
		}
		for (int i = 0; i < items.getValue1().size(); i++) {
			Ennemi en = items.getValue1().get(i);
			if(this.isVoisinItem(en)){
				System.out.println("Place au combat");
				en.combattre(this,s);
			}
		}
	}

	/**
	 * Affiche le menu des items présents sur le plateau afin de voir la vie des ennemis
	 * @param s Salle actuelle du joueur
	 */
	public void afficherMenu(Salle s){
		this.menu = new Stage();
		this.menu.setTitle("Informations");
		Group g = new Group();

		double x = 10, y = 25;
		double dy = 20;

		int nonEntityNumber = 0;
		ArrayList<ItemDesign> deco = s.getDeco(); 
		for(int i = 0; i < deco.size(); i++)
			if(deco.get(i) instanceof Wall || deco.get(i) instanceof Tile || deco.get(i).getY() == 999)
				nonEntityNumber++;
		int windowsHeight = 400 + 20 * (deco.size() - nonEntityNumber);

		Scene scene = new Scene(g, 500, windowsHeight, Color.BLACK);

		Pair<ArrayList<ItemConsommable>,ArrayList<Ennemi>> items = new Pair<ArrayList<ItemConsommable>,ArrayList<Ennemi>> (s.getObjetsCollisions().getValue0(),s.getObjetsCollisions().getValue1());
		for (int i = 0; i < items.getValue0().size();i++) {
			ItemConsommable it = items.getValue0().get(i);
			Label texte = new Label(it.toString());
			texte.setTextFill(Color.WHITE);
			texte.setLayoutX(x);
			texte.setLayoutY(y);
			y += dy;
			g.getChildren().add(texte);

		}
		for (int i = 0; i < items.getValue1().size();i++) {
			Ennemi e = items.getValue1().get(i);
			Label texte = new Label(e.toString());
			texte.setTextFill(Color.WHITE);
			texte.setLayoutX(x);
			texte.setLayoutY(y);
			y += dy;
			g.getChildren().add(texte);
		}
		//Joueur
		Label texte = new Label(this.toString());
		texte.setTextFill(Color.WHITE);
		texte.setLayoutX(x);
		texte.setLayoutY(y);
		texte.setStyle("-fx-font-weight: bold;");
		g.getChildren().add(texte);

		this.menu.setScene(scene);
		this.menu.setResizable(false);
		this.menu.show();
	}

}