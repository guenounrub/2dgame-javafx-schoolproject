package objets;

import room.Salle;

public class PotionVie extends Potion{
	protected final static String IMG = "room/imgs/skinsItems/potionVie.png";

	public PotionVie(double x,double y) {
		super(x, y, IMG);
		this.bonus = 15;
	}

	public void utiliser(Joueur j,Salle s){
		j.setVie(j.getVie()+this.bonus);
		s.rmObjet(this);
	}

	public String toString(){
		return "Potion de vie : x = " + (int)this.getX() + ", y = " + (int)this.getY() + ", Bonus de vie = " + this.getBonus();
	}

	// public void update(){}
}