package objets;

import room.Salle;

public class Fantassin extends Ennemi {

	// Images des enemmis
	private static String IMGHAUT = "room/imgs/skinEnnemis/Fantassin/up.png";
	private static String IMGBAS = "room/imgs/skinEnnemis/Fantassin/down.png";
	private static String IMGGAUCHE = "room/imgs/skinEnnemis/Fantassin/left.png";
	private static String IMGDROITE = "room/imgs/skinEnnemis/Fantassin/right.png";



	public Fantassin(double x, double y, String direction) {
		super(x, y, 100, 50, getImage(direction));
	}

	public String toString(){
		return "Fantassin : x = " + (int)this.getX() + ", y = " + (int)this.getY() + " / Vie = " + this.getVie() + ", Atk = " + this.getAtk();
	}

			/**
	 * Fonction qui choisit l'image de l'ennemi en fonction de la direction souhaité
	 * @param direction
	 * @return Le chemin de la bonne image
	 */
	private static String getImage(String direction) {
		String img = "";
		switch (direction) {
			case HAUT:
				img = IMGHAUT;
				break;
			case BAS:
				img = IMGBAS;
				break;
			case DROITE:
				img = IMGDROITE;
				break;
			case GAUCHE:
				img = IMGGAUCHE;
				break;
		}
		return img;
	}
	
	public void combattre(Joueur j,Salle s){
		if(j.getAtk() > this.getVie() || this.getVie() <= 0) {
			s.rmEnnemis(this);
		}
		else {
			j.setVie(j.getVie()-(this.getAtk()-j.getAtk()));
			this.setVie(this.getVie()-j.getAtk());
		}
		if(j.getVie() <= 0){ // On regarde si le joueur est mort à l'issue du combat
			Ennemi.afficherMenuFin(s);
		}
	}
}