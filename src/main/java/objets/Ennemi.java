package objets;

import java.io.IOException;

import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.fxml.FXMLLoader;
import javafx.stage.StageStyle;
import javafx.scene.Scene;

import room.Salle;

public abstract class Ennemi extends ItemNonConsommable {

	// Vie et attaque de l'Ennemi
	private int vie;
	private int atk;

	protected final static String HAUT = "UP", BAS = "DOWN", GAUCHE = "LEFT", DROITE = "RIGHT";


	public Ennemi(double x, double y, int vie, int atk, String path) {
		super(x, y, path);
		this.vie = vie; //90;
		this.atk = atk; //50;
	}

	public int getVie() {
		return vie;
	}

	public void setVie(int vie) {
		this.vie = vie;
	}

	public int getAtk() {
		return atk;
	}

	public void setAtk(int atk) {
		this.atk = atk;
	}

	public String toString(){
		return "Ennemi : x = " + (int)this.getX() + ", y = " + (int)this.getY() + " / Vie = " + this.getVie() + ", Atk = " + this.getAtk();
	}

	/**
	 * Fonction qui gère le combat entre un ennemi et le joueur
	 * Règle : Si l'atk du joueur est supérieure aux pts de vie de l'ennemi, l'ennemis meurt sur le coup
	 * 			Sinon : Le joueur perd des points de vie égaux à la difference entre les deux attaques et 
	 * 				l'ennemi perd des points de vie égaux à l'attaque du joueur 
	 * @param j Joueur
	 * @param s Salle actuelle
	 */
	public abstract void combattre(Joueur j,Salle s);
	
	protected static void afficherMenuFin(Salle s){
		// Stage menuFin = new Stage();
		// menuFin.initStyle(StageStyle.UNDECORATED);
		
		// try {
		// 	Parent menu = FXMLLoader.load(getClass().getResource("fxml/EcranFin.fxml"));
		// 	Scene scene = new Scene(menu);
		// 	menuFin.setScene(scene);
		// 	menuFin.show();
		// } catch (IOException ex) {
		// 	System.out.println(ex);
		// 	ex.printStackTrace();
		// }
		// Stage jeu = (Stage) s.getPlateau().getScene().getWindow();
		// jeu.close();
	}
}