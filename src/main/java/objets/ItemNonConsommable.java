package objets;

public abstract class ItemNonConsommable extends Item{

	public ItemNonConsommable(){
		super();
	}

	public ItemNonConsommable(double x,double y,String path) {
		super(x, y, path);
	}

}