package objets;

import room.Salle;


public abstract class ItemConsommable extends Item{
	
	public ItemConsommable(){
		super();
	}

	public ItemConsommable(double x,double y,String path) {
		super(x, y, path);
	}

	public abstract void ramasser(Joueur j,Salle s);
	
}