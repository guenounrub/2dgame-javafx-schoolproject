package objets;

import java.util.ArrayList;

import javafx.scene.input.KeyCode;
import javafx.scene.robot.Robot;

import room.Salle;
public class Coffre extends ItemConsommable {
	protected final static String CLOSEIMG = "room/imgs/skinsItems/coffre1.png";
	protected final static String OPENIMG = "room/imgs/skinsItems/coffre3.png";

	protected static double dx = 40;
	protected static double dy = 20;

	protected ArrayList<ItemConsommable> items;
	protected boolean isOpen;

	public Coffre(double x,double y) {
		super(x, y, CLOSEIMG);
		this.items = new ArrayList<ItemConsommable>();
		this.isOpen = false;
	}

	public Coffre(double x,double y, ItemConsommable...items) {
		super(x, y, CLOSEIMG);
		this.items = new ArrayList<ItemConsommable>();
		for (int i = 0; i < items.length; i++) {
			this.items.add(items[i]);
		}
		this.isOpen = false;
	}

	public boolean getIsOpen() {
		return this.isOpen;
	}

	public void setIsOpen(boolean isOpen) {
		this.isOpen = isOpen;
	}

	public String toString(){
		return "Coffre : x = " + (int)this.getX() + ", y = " + (int)this.getY() + ", Etat : " + (this.isOpen ? "ouvert" : "fermé");
	}

	/**
	 * Affiche les items du plateau
	 */
	public void afficherItems() {
		for (int k = 0; k < this.items.size(); k++) {
			System.out.println((k+1)+" : "+this.items.get(k));
		}
	}

	/**
	 * Ajoute un item au coffre
	 * @param i Item à ajouter
	 */
	public void addItem(ItemConsommable i){
		this.items.add(i);
	}

	/**
	 * Ajoute plusieurs items au coffre
	 * @param items Successions d'items à ajouter
	 */
	public void addItem(ItemConsommable...items){
		for (int i = 0; i < items.length; i++) {
			this.addItem(items[i]);
		}
	}

	/**
	 * Supprime un item du coffre
	 * @param i Item à supprimer
	 */
	public void rmItem(ItemConsommable i){
		if(this.items.contains(i)){
			this.items.remove(i);
		}
	}

	/**
	 * Supprime plusieurs items du coffre
	 * @param items Succession d'items à supprimer  
	 */
	public void rmItem(ItemConsommable...items){
		for (int i = 0; i < items.length; i++) {
			this.rmItem(items[i]);
		}
	}

	public void ramasser(Joueur j,Salle s){
		Robot r = new Robot();
		if (!this.isOpen){
			r.keyPress(KeyCode.LEFT);
			r.keyRelease(KeyCode.LEFT);
			r.keyPress(KeyCode.LEFT);
			r.keyRelease(KeyCode.LEFT);
			r.keyPress(KeyCode.LEFT);
			r.keyRelease(KeyCode.LEFT);
			r.keyPress(KeyCode.RIGHT);
			r.keyRelease(KeyCode.RIGHT);
			for (int i = 0; i < this.items.size();i++) {
				ItemConsommable it = this.items.get(i);
				s.addObjet(it);
				it.getItem().relocate(it.getX(),it.getY());
				it.ramasser(j,s);
				System.out.println("Objet ramassé : "+it);
			}
			this.isOpen = true;
			this.setImage(OPENIMG);
			this.getItem().relocate(this.getX(), this.getY());

			//Rajouter une fenetre avec les objets ramassés.
		}
	}



	// public void update(){}
}