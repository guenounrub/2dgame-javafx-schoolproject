package objets;

import room.Salle;


public class PotionAtk extends Potion{
	protected final static String IMG = "room/imgs/skinsItems/potionAtk.png";

	public PotionAtk(double x,double y) {
		super(x, y, IMG);
	}

	public void utiliser(Joueur j,Salle s){
		j.setAtk(j.getAtk()+this.bonus);
		s.rmObjet(this);
	}

	public String toString(){
		return "Potion d'attaque : x = " + (int)this.getX() + ", y = " + (int)this.getY() + ", Bonus d'attaque = " + this.getBonus();
	}

	// public void update(){}
}