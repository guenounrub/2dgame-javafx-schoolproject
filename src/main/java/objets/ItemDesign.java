package objets;

public abstract class ItemDesign extends Item{

	public ItemDesign(){
		super();
	}

	public ItemDesign(double x,double y,String path) {
		super(x, y, path);
	}
}