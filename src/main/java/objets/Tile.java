package objets;

public class Tile extends ItemDesign{

    public Tile(double x, double y, String path) {
        super(x, y, path);
    }

    public String getPath(String type) {
        String path = "";
        switch (type) {
            case "floor":
                path = "room/imgs/skinsItems/block.bmp";
                break;
            case "black":
                path = "room/imgs/skinsItems/block.bmp";
                break;
            case "MurB":
                path = "room/imgs/Walls/MurB.png";
                break;
            case "MurD":
                path = "room/imgs/Walls/MurD.png";
                break;
            case "MurDH":
                path = "room/imgs/Walls/MurDH.png";
                break;
            case "MurG":
                path = "room/imgs/Walls/MurG.png";
                break;
            case "MurGH":
                path = "room/imgs/Walls/MurGH.png";
                break;
            case "MurH":
                path = "room/imgs/Walls/MurTop.png";
                break;
            case "MurH2":
                path = "room/imgs/Walls/MurTop2.png";
                break;
            case "MurVe":
                path = "room/imgs/Walls/MurVe.png";
                break;
            case "MurVeB":
                path = "room/imgs/Walls/MurVeB.png";
                break;
            case "MurVeH":
                path = "room/imgs/Walls/MurVeH.png";
                break;
            default:
                path = "test";
                break;
        }
        System.out.println(path);
        return path;
    }
}
