package objets;

import room.Salle;

public abstract class Potion extends ItemConsommable{
	
	protected int bonus;

	public Potion(){
		super();
		this.bonus = 0;
	}

	public Potion(double x,double y,String path) {
		super(x, y, path);
		this.bonus = 15;
	}

	public int getBonus(){
		return this.bonus;
	}

	//Implémentation de ramasser pour la classe
	public void ramasser(Joueur j, Salle s){
		this.utiliser(j,s);
	}

	//Changement de nomage pour correspondre plus à l'objet
	public abstract void utiliser(Joueur j, Salle s);

}