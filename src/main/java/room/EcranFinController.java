package room;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import objets.Joueur;



public class EcranFinController implements Initializable {

    @FXML
    AnchorPane root;

    private final static String HAUT = "UP", BAS = "DOWN", GAUCHE = "LEFT", DROITE = "RIGHT";

    public static Salle groupToSalle(Group g,Salle...s){
        Salle tmp = null;
        for (int i = 0; i < s.length; i++) {
            if(g.equals(s[i].getPlateau())){
                tmp = s[i];
            }
        }
        return tmp;
    }

    @FXML
    protected void Action() throws IOException {
        double W = 768, H = 512;
        Stage stage = new Stage();

        room.Map map = new room.Map();
        room.Salle[][] liste_salles = new room.Salle[room.Map.NB_BLOCK_WIDTH/2][room.Map.NB_BLOCK_HEIGHT/2];

        for(int i = 0; i < liste_salles.length; i++)
            for (int j = 0; j < liste_salles.length; j++)
                liste_salles[i][j] = new room.Salle(i, j);

        for(int i = 0; i < liste_salles.length; i++)
            for (int j = 0; j < liste_salles.length; j++)
                liste_salles[i][j].addDoors(liste_salles, map.getDoors(i, j));

        Joueur j = Joueur.getInstance();
        liste_salles[0][0].setJoueur(j);
        Scene scene = new Scene(liste_salles[0][0].getPlateau(), W, H, Color.TRANSPARENT);

        stage.setScene(scene);

        /** Pour le déplacement */
        scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                switch (event.getCode()) {
                    case UP:
                    case Z:
                        j.move(HAUT,groupToSalle((Group)scene.getRoot(), liste_salles[0][0], liste_salles[0][1], liste_salles[0][2], liste_salles[0][3], liste_salles[0][4], liste_salles[1][0], liste_salles[1][1], liste_salles[1][2], liste_salles[1][3], liste_salles[1][4], liste_salles[2][0], liste_salles[2][1], liste_salles[2][2], liste_salles[2][3], liste_salles[2][4], liste_salles[3][0], liste_salles[3][1], liste_salles[3][2], liste_salles[3][3], liste_salles[3][4], liste_salles[4][0], liste_salles[4][1], liste_salles[4][2], liste_salles[4][3], liste_salles[4][4]));
                        break;
                    case DOWN:
                    case S:
                        j.move(BAS,groupToSalle((Group)scene.getRoot(), liste_salles[0][0], liste_salles[0][1], liste_salles[0][2], liste_salles[0][3], liste_salles[0][4], liste_salles[1][0], liste_salles[1][1], liste_salles[1][2], liste_salles[1][3], liste_salles[1][4], liste_salles[2][0], liste_salles[2][1], liste_salles[2][2], liste_salles[2][3], liste_salles[2][4], liste_salles[3][0], liste_salles[3][1], liste_salles[3][2], liste_salles[3][3], liste_salles[3][4], liste_salles[4][0], liste_salles[4][1], liste_salles[4][2], liste_salles[4][3], liste_salles[4][4]));
                        break;
                    case LEFT:
                    case Q:
                        j.move(GAUCHE,groupToSalle((Group)scene.getRoot(), liste_salles[0][0], liste_salles[0][1], liste_salles[0][2], liste_salles[0][3], liste_salles[0][4], liste_salles[1][0], liste_salles[1][1], liste_salles[1][2], liste_salles[1][3], liste_salles[1][4], liste_salles[2][0], liste_salles[2][1], liste_salles[2][2], liste_salles[2][3], liste_salles[2][4], liste_salles[3][0], liste_salles[3][1], liste_salles[3][2], liste_salles[3][3], liste_salles[3][4], liste_salles[4][0], liste_salles[4][1], liste_salles[4][2], liste_salles[4][3], liste_salles[4][4]));
                        break;
                    case RIGHT:
                    case D:
                        j.move(DROITE,groupToSalle((Group)scene.getRoot(), liste_salles[0][0], liste_salles[0][1], liste_salles[0][2], liste_salles[0][3], liste_salles[0][4], liste_salles[1][0], liste_salles[1][1], liste_salles[1][2], liste_salles[1][3], liste_salles[1][4], liste_salles[2][0], liste_salles[2][1], liste_salles[2][2], liste_salles[2][3], liste_salles[2][4], liste_salles[3][0], liste_salles[3][1], liste_salles[3][2], liste_salles[3][3], liste_salles[3][4], liste_salles[4][0], liste_salles[4][1], liste_salles[4][2], liste_salles[4][3], liste_salles[4][4]));
                        break;
                    case SPACE:
                        j.useItem(groupToSalle((Group)scene.getRoot(), liste_salles[0][0], liste_salles[0][1], liste_salles[0][2], liste_salles[0][3], liste_salles[0][4], liste_salles[1][0], liste_salles[1][1], liste_salles[1][2], liste_salles[1][3], liste_salles[1][4], liste_salles[2][0], liste_salles[2][1], liste_salles[2][2], liste_salles[2][3], liste_salles[2][4], liste_salles[3][0], liste_salles[3][1], liste_salles[3][2], liste_salles[3][3], liste_salles[3][4], liste_salles[4][0], liste_salles[4][1], liste_salles[4][2], liste_salles[4][3], liste_salles[4][4]));
                        break;
                    case CONTROL:
                        j.afficherMenu(groupToSalle((Group)scene.getRoot(), liste_salles[0][0], liste_salles[0][1], liste_salles[0][2], liste_salles[0][3], liste_salles[0][4], liste_salles[1][0], liste_salles[1][1], liste_salles[1][2], liste_salles[1][3], liste_salles[1][4], liste_salles[2][0], liste_salles[2][1], liste_salles[2][2], liste_salles[2][3], liste_salles[2][4], liste_salles[3][0], liste_salles[3][1], liste_salles[3][2], liste_salles[3][3], liste_salles[3][4], liste_salles[4][0], liste_salles[4][1], liste_salles[4][2], liste_salles[4][3], liste_salles[4][4]));
                        break;
                    case ESCAPE:
                        Stage menuPause = new Stage();
                        menuPause.initStyle(StageStyle.UNDECORATED);
                        Parent menu;
                        try {
                            menu = FXMLLoader.load(getClass().getResource("fxml/MenuPause.fxml"));
                            Scene scene = new Scene(menu);
                            menuPause.setScene(scene);
                            menuPause.show();
                        } catch (IOException e) {
                            System.out.println(e);
                            //e.printStackTrace();
                        }
                }
            }
        });

        stage.show();

        //ferme la fenetre du menu
        Stage parent = (Stage) root.getScene().getWindow();
        parent.close();
    }

    @FXML
    protected void RetourMenu() throws IOException {
        AnchorPane B=FXMLLoader.load(getClass().getResource("/Menu/fxml/Menu.fxml"));
        root.getChildren().setAll(B);
    }

    @FXML private javafx.scene.control.Button closeButton;

    @FXML
    private void closeButtonAction(){
        Stage stage = (Stage) closeButton.getScene().getWindow();
        stage.close();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }


}