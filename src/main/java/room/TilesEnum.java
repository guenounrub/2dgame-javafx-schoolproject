package room;

public enum TilesEnum {
    BORDER,
    TILE,
    DECORATION_TILE_1, // UNUSED
    DECORATION_TILE_2, // UNUSED
    DECORATION_TILE_3, // UNUSED
    WALL,
    ITEM, // UNUSED
    DOOR, // UNUSED
    LOCKED_DOOR // UNUSED
}
