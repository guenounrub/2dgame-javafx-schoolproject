package room;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Stack;
import java.util.concurrent.ThreadLocalRandom;

import javax.imageio.ImageIO;



public class Map
{
    // Taille du labyrinthe de base servant à générer la map
    public static final int NB_BLOCK_WIDTH = 11;
    public static final int NB_BLOCK_HEIGHT = 11;

    private TilesEnum[][] map; // Map principale (OBSOLETE)
    private TilesEnum[][] map_laby; // Labyrinthe servant à construire la map
    private int[][] tiles_id; // Tableau d'identifiants des cases servant à construire le labyrinthe

    // Le repère des tableaux 2D fonctionne comme suit :
    // Origine (0, 0) dans le coin supérieur gauche
    // En se déplaçant vers la droite, les x augmentent (comme dans un repère cartésien)
    // En revanche, les y augmentent lorsqu'on se déplace vers le bas

    public Map() {
        map = new TilesEnum[200][200]; // OBSOLETE
        map_laby = new TilesEnum[NB_BLOCK_WIDTH][NB_BLOCK_HEIGHT];
        tiles_id = new int[NB_BLOCK_WIDTH][NB_BLOCK_HEIGHT];

        initializeLabyrinthe();
        initializeMap();
        //createMapImage();
    }

    /* #####################################################################
     * ########## Fonctions servant à la génération du labyrinthe ##########
     * #####################################################################
     */

    /* On initialise le labyrinthe et ses identifiants avec 3 valeurs :
    - Bordure de la map = mur incassable, identifiant -1
    - Sur toutes les cases de coordonnées impaires : case vide d'identifiant croissant. Le but est de créer une grille au départ.
    - Partout ailleurs : mur d'identifiant 0.
     */
    public void initializeLabyrinthe() {
        int id = 100;
        for(int y = 0; y < NB_BLOCK_HEIGHT; y++) {
            for(int x = 0; x < NB_BLOCK_WIDTH; x++) {
                if(x == 0 || x == NB_BLOCK_WIDTH-1 || y == 0 || y == NB_BLOCK_HEIGHT-1) {
                    map_laby[x][y] = TilesEnum.BORDER;
                    tiles_id[x][y] = -1;
                }
                else if(x%2 == 1 && y%2 == 1) {
                    map_laby[x][y] = TilesEnum.TILE;
                    tiles_id[x][y] = id;
                    id++;
                }
                else {
                    map_laby[x][y] = TilesEnum.WALL;
                    tiles_id[x][y] = 0;
                }
            }
        }

        generateLabyrinth();
    }

    // Fonction basique que Java ne semble pas posséder
    public static int randint(int min, int max) {
        return ThreadLocalRandom.current().nextInt(min, max+1);
    }

    /* Traduction de l'algorithme présenté ici : https://fr.wikipedia.org/wiki/Mod%C3%A9lisation_math%C3%A9matique_de_labyrinthe#Algorithmes_de_construction_de_labyrinthes
    Résumé : chaque case possède un identifiant unique. A chaque itération, on choisit une case et une direction aléatoires, et on tente d'ouvrir le mur.
    Si la case derrière ce mur a un identifiant différent de la case sur laquelle on s'est placé, cela signifie qu'elle n'est pas sur le même chemin et que l'on peut ouvrir.
    Si l'identifiant est identique, les deux cases sont déjà reliées par un chemin et on n'ouvre pas.
    Après ouverture d'un mur, on appelle la fonction fill_id qui parcourera l'intégralité du chemin créé afin de tout mettre au même identifiant.
     */
    public void generateLabyrinth() {
        int iteration = 0;
        while(iteration < 0.25*(NB_BLOCK_HEIGHT-1)*(NB_BLOCK_WIDTH-1)-1){
            // Coordonnées et direction aléatoire
            // La forme du randint est telle que l'on obtienne deux coordonnées impaires pour tomber sur une case vide de la grille.
            int x = 2 * randint(0, NB_BLOCK_WIDTH/2 - 1) + 1;
            int y = 2 * randint(0, NB_BLOCK_HEIGHT/2 - 1) + 1;
            int direction = randint(0, 3);

            // Si l'ouverture réussit, on remplace le mur par une case vide, on change l'identifiant de l'ex-mur et de la case d'après, et on appelle fill_id à partir de la case d'après.
            if(direction == Direction.HAUT.value && verif_id(x, y, direction)) {
                map_laby[x][y-1] = TilesEnum.TILE;
                tiles_id[x][y-1] = tiles_id[x][y];
                tiles_id[x][y-2] = tiles_id[x][y];
                fill_id(x, y-2);
                iteration++;
            }
            else if(direction == Direction.DROITE.value && verif_id(x, y, direction)) {
                map_laby[x+1][y] = TilesEnum.TILE;
                tiles_id[x+1][y] = tiles_id[x][y];
                tiles_id[x+2][y] = tiles_id[x][y];
                fill_id(x+2, y);
                iteration++;
            }
            else if(direction == Direction.BAS.value && verif_id(x, y, direction)) {
                map_laby[x][y+1] = TilesEnum.TILE;
                tiles_id[x][y+1] = tiles_id[x][y];
                tiles_id[x][y+2] = tiles_id[x][y];
                fill_id(x, y+2);
                iteration++;
            }
            else if(direction == Direction.GAUCHE.value && verif_id(x, y, direction)) {
                map_laby[x-1][y] = TilesEnum.TILE;
                tiles_id[x-1][y] = tiles_id[x][y];
                tiles_id[x-2][y] = tiles_id[x][y];
                fill_id(x-2, y);
                iteration++;
            }
        }
    }

    /* Fonction vérifiant la validité du mur que l'on veut ouvrir.
    Paramètres : les coordonnées de la case où l'on se trouve et la direction générée
    La fonction vérifie si la case que l'on souhaite ouvrir est bien un mur (identifiant 0) et si la case derrière ce mur n'a pas un identifiant identique à la case de départ.
     */
    public boolean verif_id(int x, int y, int dir)
    {
        if(dir == Direction.HAUT.value && tiles_id[x][y-1] == 0 && tiles_id[x][y-2] != tiles_id[x][y])
            return true;
        else if(dir == Direction.DROITE.value && tiles_id[x+1][y] == 0 && tiles_id[x+2][y] != tiles_id[x][y])
            return true;
        else if(dir == Direction.BAS.value && tiles_id[x][y+1] == 0 && tiles_id[x][y+2] != tiles_id[x][y])
            return true;
        else if(dir == Direction.GAUCHE.value && tiles_id[x-1][y] == 0 && tiles_id[x-2][y] != tiles_id[x][y])
            return true;

        return false;
    }

    /* Fonction servant à remplir toutes les cases d'un chemin avec le même identifiant.
    Paramètres : les coordonnées du point de départ. Ils seront utilisés ensuite comme "curseurs" afin de se déplacer dans le labyrinthe.
     */
    public void fill_id(int x, int y)
    {
        // On crée deux piles pour ranger les coordonnées des intersections.
        Stack<Integer> intersections_x = new Stack<Integer>();
        Stack<Integer> intersections_y = new Stack<Integer>();

        // On ajoute les coordonnées du départ au cas où la case de départ se trouve déjà sur une intersection.
        // Le nombre de chemins d'une intersection est donné par la fonction check_intersection
        for(int i = 0; i < check_intersection(x, y); i++){
            intersections_x.push(x);
            intersections_y.push(y);
        }

        int intersection = 0;
        int idToFill = tiles_id[x][y]; // Valeur de l'identifiant à remplir sur le chemin

        do{
            intersection = check_intersection(x, y); // On calcule le nombre de chemins sur la case actuelle

            //Clarification : un simple virage ne présente pas d'intersection, mais la valeur de la variable intersection sera de 1 car il existe un chemin
            //S'il y a au moins deux chemins sur la case actuelle et que le sommet de la pile ne contient pas déjà les coordonnées de l'intersection, on l'ajoute
            if(intersection >= 2 && !(x == intersections_x.peek()  && y == intersections_y.peek())) {
                for(int i = 0; i < intersection; i++){
                    intersections_x.push(x);
                    intersections_y.push(y);
                }
            }
            // S'il n'y a aucun chemin (on est arrivé dans un cul-de-sac), on se place aux coordonnées de la précédente intersection et on les retire de la pile.
            else if(intersection == 0 && !intersections_x.empty() && !intersections_y.empty()) {
                x = intersections_x.pop();
                y = intersections_y.pop();
            }

            // On change l'identifiant des cases avec comme condition que ce sont bien des cases vides (pas des murs) et que leur identifiant est différent
            // Puis, on change la valeur du curseur pour traduire le déplacement fait.
            if(tiles_id[x][y-1] > 0 && tiles_id[x][y-1] != idToFill){
                tiles_id[x][y-1] = idToFill;
                tiles_id[x][y-2] = idToFill;
                y -= 2;
            }
            else if(tiles_id[x+1][y] > 0 && tiles_id[x+1][y] != idToFill){
                tiles_id[x+1][y] = idToFill;
                tiles_id[x+2][y] = idToFill;
                x += 2;
            }
            else if(tiles_id[x][y+1] > 0 && tiles_id[x][y+1] != idToFill){
                tiles_id[x][y+1] = idToFill;
                tiles_id[x][y+2] = idToFill;
                y += 2;
            }
            else if(tiles_id[x-1][y] > 0 && tiles_id[x-1][y] != idToFill){
                tiles_id[x-1][y] = idToFill;
                tiles_id[x-2][y] = idToFill;
                x -= 2;
            }

        } while(!intersections_x.empty() && !intersections_y.empty());
    }

    // Renvoie le nombre de chemins aux coordonnées (x, y)
    public int check_intersection(int x, int y)
    {
        int path = 0;
        if(tiles_id[x][y-1] != tiles_id[x][y] && tiles_id[x][y-1] > 0) //HAUT
            path++;
        if(tiles_id[x+1][y] != tiles_id[x][y] && tiles_id[x+1][y] > 0) //DROITE
            path++;
        if(tiles_id[x][y+1] != tiles_id[x][y] && tiles_id[x][y+1] > 0) //BAS
            path++;
        if(tiles_id[x-1][y] != tiles_id[x][y] && tiles_id[x-1][y] > 0) //GAUCHE
            path++;
        return path;
    }

    /* #####################################################################
     * ############ Fonctions servant à la génération de la map ############
     * #####################################################################
     */

    // Cette partie est maintenant obsolète suite à un changement de décision.

    // On initialise la map avec des murs partout.
    public void initializeMap() {
        for(int i = 0; i < 200; i++)
            for(int j = 0; j < 200; j++)
                map[i][j] = TilesEnum.WALL;

        generateMap();
    }

    public void generateMap() {
        // Variables servant à se déplacer dans la grille
        int cursor_x = 1;
        int cursor_y = 1;

        for(int y = 0; y < NB_BLOCK_HEIGHT/2; y++) {
            cursor_x = 1;
            cursor_y = y*10+1;
            for(int x = 0; x < NB_BLOCK_WIDTH/2; x++) {
                Point p1 = new Point(cursor_x, cursor_y);
                Point p2 = new Point(p1.getX() + 7, p1.getY() + 7);

                // Création de la salle
                for (int i = p1.getX(); i < p2.getX(); i++) {
                    for (int j = p1.getY(); j < p2.getY(); j++)
                        map[i][j] = TilesEnum.TILE;
                }

                // Génération des couloirs horizontaux
                if(map_laby[2*(x+1)][2*y+1] == TilesEnum.TILE) {
                    for(int k = 0; k < 6; k++) {
                        map[cursor_x + 7 + k][cursor_y + 3] = TilesEnum.TILE;
                    }
                }

                // Génération des couloirs verticaux
                if(map_laby[2*x+1][2*y] == TilesEnum.TILE && y != 0) {
                    for(int k = 0; k < 6; k++) {
                        map[cursor_x + 3][cursor_y - k] = TilesEnum.TILE;
                    }
                }

                cursor_x += 10;
            }
        }

    }

    public char[] getDoors(int x, int y) {
        char[] directions = new char[4];
        directions[0] = '0';
        directions[1] = '0';
        directions[2] = '0';
        directions[3] = '0';
        x = 2*x+1;
        y = 2*y+1;

        if(this.getLaby(x, y-1) == TilesEnum.TILE)
            directions[0] = 'H';
        if(this.getLaby(x+1, y) == TilesEnum.TILE)
            directions[1] = 'D';
        if(this.getLaby(x, y+1) == TilesEnum.TILE)
            directions[2] = 'B';
        if(this.getLaby(x-1, y) == TilesEnum.TILE)
            directions[3] = 'G';

        return directions;
    }


    // Renvoie la case du labyrinthe aux coordonnées (x, y)
    public TilesEnum getLaby(int x, int y) {
        return map_laby[x][y];
    }

    // Renvoie la case de la map correspondant aux coordonnées (x, y)
    public TilesEnum getTile(int x, int y) {
        return map[x][y];
    }



    public void createMapImage() {
        BufferedImage emptyMap = null;
        try {
            emptyMap = ImageIO.read(new File("../../src/main/resources/room/imgs/emptyMap.jpg"));
        } catch(IOException e) {
            System.err.println("Erreur lors de l'ouverture du fichier : " + e);
        }

        Graphics g = emptyMap.createGraphics();

        // Génération des contours des salles
        g.setColor(Color.DARK_GRAY);
        for(int i = 0; i < 5; i++)
            for(int j = 0; j < 5; j++)
                g.fillRect(26+i*55, 26+j*55, 44, 44);

        // Génération de l'intérieur des salles
        g.setColor(Color.GRAY);
        for(int i = 0; i < 5; i++)
            for(int j = 0; j < 5; j++)
                g.fillRect(29+i*55, 29+j*55, 38, 38);

        // Génération  des contours des couloirs
        g.setColor(Color.DARK_GRAY);
        for(int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                if(getLaby(2*(i+1), 2*j+1) == TilesEnum.TILE)
                    g.fillRect(65+i*55, 42+j*55, 21, 12);
                if(getLaby(2*i+1, 2*j) ==  TilesEnum.TILE)
                    g.fillRect(42+i*55, 10+j*55, 12, 22);
            }
        }

        // Génération de l'intérieur des couloirs
        g.setColor(Color.GRAY);
        for(int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                if(getLaby(2*(i+1), 2*j+1) == TilesEnum.TILE)
                    g.fillRect(65+i*55, 44+j*55, 21, 8);
                if(getLaby(2*i+1, 2*j) ==  TilesEnum.TILE)
                    g.fillRect(44+i*55, 10+j*55, 8, 22);
            }
        }

        g.dispose();

        try {
            ImageIO.write(emptyMap, "jpg", new File("../../src/main/resources/room/imgs/mapImg.jpg"));
        } catch(IOException e) {
            System.err.println("Erreur lors de l'ecriture du fichier : " + e);
        }
    }

}
