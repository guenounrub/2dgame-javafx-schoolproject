package room;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;



public class MenuPauseController implements Initializable {

    @FXML
    AnchorPane root;

    @FXML
    protected void Action() throws IOException {
        Stage stage = (Stage) closeButton.getScene().getWindow();
        stage.close();
        
    }

    @FXML
    protected void Controle() throws IOException {
        AnchorPane B=FXMLLoader.load(getClass().getResource("/MenuPause/fxml/Controletouch2.fxml"));
        root.getChildren().setAll(B);
        
    }

    @FXML
    protected void RetourMenu() throws IOException {
        AnchorPane C=FXMLLoader.load(getClass().getResource("/Menu/fxml/MenuPause.fxml"));
        root.getChildren().setAll(C);
        
    }


    @FXML
    protected void PageEnnemis() throws IOException {
        AnchorPane D=FXMLLoader.load(getClass().getResource("/MenuPause/fxml/Ennemis.fxml"));
        root.getChildren().setAll(D);
        
    }
    @FXML
    protected void Combat() throws IOException {
        AnchorPane F=FXMLLoader.load(getClass().getResource("/MenuPause/fxml/Combat.fxml"));
        root.getChildren().setAll(F);
        
    }

     @FXML
    protected void Guide() throws IOException {
        AnchorPane E=FXMLLoader.load(getClass().getResource("/MenuPause/fxml/Guide2.fxml"));
        root.getChildren().setAll(E);
        
    }


    @FXML private javafx.scene.control.Button closeButton; 

    @FXML
    private void closeButtonAction(){
        javafx.application.Platform.exit();
    }
    
    



    @Override
    public void initialize(URL location, ResourceBundle resources) {
       

    }

    
}


