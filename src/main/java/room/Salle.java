package room;

import java.util.ArrayList;

import javafx.scene.Group;
import objets.*;
import org.javatuples.Pair;
import org.javatuples.Triplet;

public class Salle {

	private Group plateau;

	//Items présents dans une salle
	private ArrayList<ItemDesign> decoList;
	private ArrayList<Ennemi> ennemisList;
	private ArrayList<ItemConsommable> objetsList;
	private Joueur j;

	private int x,y;

	/* La difficulté donne le nombre d'ennemis et d'items qui seront générés dans la salle
	 * 0 = salle facile (40%)
	 * 1 = salle moyenne (40%)
	 * 2 = salle difficile (20%)
	 */
	int difficulty;

	private boolean isBonus;

	public Salle(int x, int y) {
		this.x = x;
		this.y = y;
		this.plateau = new Group();
		this.decoList = new ArrayList<ItemDesign>();
		this.ennemisList = new ArrayList<Ennemi>();
		this.objetsList = new ArrayList<ItemConsommable>();
		this.isBonus = false;
		this.j = null;

		double r = Math.random();
		this.difficulty = r <= 0.4 ? 0 : (r <= 0.8 ? 1 : 2);

		generateRoom();
	}


	//--------------------- Getters / Setters ----------------//

	public boolean isBonus(){
		return this.isBonus;
	}

	public void setBonus(){
		this.isBonus = true;
	}

	public void unsetBonus(){
		this.isBonus = false;
	}

	public Group getPlateau(){
		return this.plateau;
	}

	public ArrayList<ItemConsommable> getObjets(){
		return this.objetsList;
	}
	
	public ArrayList<Ennemi> getEnnemis(){
		return this.ennemisList;
	}

	public ArrayList<ItemDesign> getDeco(){
		return this.decoList;
	}

	public void setJoueur(Joueur j){
		this.j = j;
		this.plateau.getChildren().add(this.j.getItem());
		this.j.getItem().relocate(this.j.getX(),this.j.getY());
	}

	public Joueur getJoueur(){
		return this.j;
	}

	public void rmJoueur(Joueur j){
		this.plateau.getChildren().remove(j.getItem());
	}

	//------------- Gestion des listes --------------//
	
	// 1. Fonctions pour decoList

	public void addDeco(ItemDesign i){
		this.decoList.add(i);
		this.plateau.getChildren().add(i.getItem());
		i.getItem().relocate(i.getX(),i.getY());
	}

	public void addDeco(ItemDesign...items){
		for (int i = 0; i < items.length; i++) {
			this.addDeco(items[i]);
		}
	}

	public void rmDeco(ItemDesign i){
		this.plateau.getChildren().remove(i.getItem());
		this.decoList.remove(i);
	}

	public void rmDeco(ItemDesign...items){
		for (int i = 0; i < items.length; i++) {
			this.rmDeco(items[i]);
		}
	}


	// 2. Fonctions pour ennemisList
	
	public void addEnnemis(Ennemi e){
		this.ennemisList.add(e);
		this.plateau.getChildren().add(e.getItem());
		e.getItem().relocate(e.getX(),e.getY());
	}

	public void addEnnemis(Ennemi...ennemis){
		for (int i = 0; i < ennemis.length; i++) {
			this.addEnnemis(ennemis[i]);
		}
	}

	public void rmEnnemis(Ennemi e){
		System.out.println("In rm ennemis avec "+e);
		this.plateau.getChildren().remove(e.getItem());
		this.ennemisList.remove(e);
		System.out.println("Après suppréssion, Liste ennemis :");
		this .afficherEnnemis();
	}

	public void rmEnnemis(Ennemi...ennemis){
		for (int i = 0; i < ennemis.length; i++) {
			this.rmEnnemis(ennemis[i]);
		}
	}

	/**
	 * Retire un Ennemi de la liste afin de vérifier le voisinage 
	 * @param e Ennemi à retirer provisoirement
	 * @return Liste d'item sans l'item i Mais ne modifie pas la scène
	 */
	public ArrayList<Ennemi> retirerEnnemiListe(Ennemi e){
		ArrayList<Ennemi> tmp = new ArrayList<Ennemi>();
		for (int j = 0; j < this.ennemisList.size(); j++) {
			Ennemi en = this.ennemisList.get(j);
			if(!en.equals(e)){
				tmp.add(en);
			}
		}
		return tmp;
	}

	/**
	 * Affiche les items du plateau
	 */
	public void afficherEnnemis(){
		for (int k = 0; k < this.ennemisList.size(); k++) {
			System.out.println((k+1)+" : "+this.ennemisList.get(k));
		}
	}



	// 3. Fonctions pour objetsList

	public void addObjet(ItemConsommable i){
		this.objetsList.add(i);
		this.plateau.getChildren().add(i.getItem());
		i.getItem().relocate(i.getX(),i.getY());
	}

	public void addObjet(ItemConsommable...items){
		for (int i = 0; i < items.length; i++) {
			this.addObjet(items[i]);
		}
	}

	public void rmObjet(ItemConsommable i){
		this.plateau.getChildren().remove(i.getItem());
		this.objetsList.remove(i);
	}

	public void rmObjet(ItemConsommable...items){
		for (int i = 0; i < items.length; i++) {
			this.rmObjet(items[i]);
		}
	}

		/**
	 * Retire un Objet de la liste afin de vérifier le voisinage 
	 * @param i objet à retirer provisoirement
	 * @return Liste d'item sans l'item i Mais ne modifie pas la scène
	 */
	public ArrayList<ItemConsommable> retirerItemListe(ItemConsommable i){
		ArrayList<ItemConsommable> tmp = new ArrayList<ItemConsommable>();
		for (int j = 0; j < this.objetsList.size(); j++) {
			ItemConsommable item = this.objetsList.get(j);
			if(!item.equals(i)){
				tmp.add(item);
			}
		}
		return tmp;
	}

	/**
	 * Affiche les items du plateau
	 */
	public void afficherObjets(){
		for (int k = 0; k < this.objetsList.size(); k++) {
			System.out.println((k+1)+" : "+this.objetsList.get(k));
		}
	}


	/* 4. Autre sfonction de manipulation des listes */
	
	//Renvoi une liste avec les objets consommables et les ennemis pour les collisions
	// public ArrayList<Item>[] getObjetsCollisions(){
	// 	ArrayList<Item>[] tmp = new ArrayList<Item>[2];
	// 	tmp[0] = this.objetsList;
	// 	tmp[1] = this.ennemisList;
	// 	return tmp;
	// }

	// public Pair<ArrayList<ItemConsommable>,ArrayList<Ennemi>> getObjetsCollisions(){
	// 	Pair<ArrayList<ItemConsommable>,ArrayList<Ennemi>> tmp = new Pair<ArrayList<ItemConsommable>,ArrayList<Ennemi>>(this.objetsList,this.ennemisList);
	// 	return tmp;
	// }

	public Triplet<ArrayList<ItemConsommable>,ArrayList<Ennemi>,ArrayList<ItemDesign>> getObjetsCollisions(){
		Triplet<ArrayList<ItemConsommable>,ArrayList<Ennemi>,ArrayList<ItemDesign>> tmp;
		tmp = new Triplet<ArrayList<ItemConsommable>,ArrayList<Ennemi>,ArrayList<ItemDesign>>(this.objetsList,this.ennemisList,this.decoList);
		return tmp;
	}


	//------------- Génération/Création de la salle ----------------//


	//Génère une salle
	public void generateRoom() {
		addWalls();
		if(this.x != 4 || this.y != 4) {
			addDecorationWalls();
			addItems();
			ajouterEnnemis();
		}
		else { // Salle du boss
			Boss boss = new Boss(384, 256, "UP");
			this.addEnnemis(boss);
		}
	}

	/* Partie décoration */

	//Ajoute les murs à la salle
	public void addWalls() {
		// Murs sur la bordure
		// Les tests servent à savoir quel type de mur on veut placer afin de donner une impression de perspective.
		for(int i = 0; i < 24; i++) {
			for (int j = 0; j < 16; j++) {
				if (i == 0 || j == 0 || i == 23 || j == 15)
					this.addDeco(new Wall(i * 32, j * 32, "room/imgs/Walls/BlockNoir.png"));
				else if(i == 1)
					this.addDeco(new Wall(i * 32, j * 32, "room/imgs/Walls/MurG.png"));
				else if(i == 22)
					this.addDeco(new Wall(i * 32, j * 32, "room/imgs/Walls/MurD.png"));
				else if(j == 1 && i % 2 == 1)
					this.addDeco(new Wall(i * 32, j * 32, "room/imgs/Walls/MurTop.png"));
				else if(j == 1 && i % 2 == 0)
					this.addDeco(new Wall(i * 32, j * 32, "room/imgs/Walls/MurTop2.png"));
				else if(j == 14)
					this.addDeco(new Wall(i * 32, j * 32, "room/imgs/Walls/MurB.png"));
			}
		}
		this.addDeco(new Wall(1 * 32, 14 * 32, "room/imgs/Walls/BlockNoir.png"));
		this.addDeco(new Wall(22 * 32, 14 * 32, "room/imgs/Walls/BlockNoir.png"));

		// Remplissage de l'intérieur de la salle avec le sol
		for(int i = 2; i < 22; i++)
			for(int j = 2; j < 14; j++)
				this.addDeco(new Tile(i * 32, j * 32, "room/imgs/Walls/floor.png"));
	}

	public void addDecorationWalls() {
		// On ajoute ou non des murs de décorations dans la salle pour varier un peu
		// Les nombreux tests servent également à placer le bon sprite de mur
		double randomWallPattern = Math.random();

		if (randomWallPattern <= 0.166666) { // Mur rectangulaire au centre
			for (int i = 0; i < 6; i++) {
				for (int j = 0; j < 4; j++) {
					if(j == 3)
						this.addDeco(new Wall(288 + i * 32, 192 + j * 32, "room/imgs/Walls/MurTop.png"));
					else if(j == 0)
						this.addDeco(new Wall(288 + i * 32, 192 + j * 32, "room/imgs/Walls/MurB.png"));
					else if(i == 0)
						this.addDeco(new Wall(288 + i * 32, 192 + j * 32, "room/imgs/Walls/MurD.png"));
					else if(i == 5)
						this.addDeco(new Wall(288 + i * 32, 192 + j * 32, "room/imgs/Walls/MurG.png"));
					else
						this.addDeco(new Wall(288 + i * 32, 192 + j * 32, "room/imgs/Walls/BlockNoir.png"));

					if(i == 0 && j == 0)
						this.addDeco(new Wall(288 + i * 32, 192 + j * 32, "room/imgs/Walls/MurGH.png"));
					else if(i == 5 && j == 0)
						this.addDeco(new Wall(288 + i * 32, 192 + j * 32, "room/imgs/Walls/MurDH.png"));
				}
			}
		}
		else if (randomWallPattern <= 0.333333) { // Mur en U au centre
			for (int k = 0; k < 4; k++) {
				this.addDeco(new Wall(288, 192 + k * 32, "room/imgs/Walls/MurVe.png"));
				this.addDeco(new Wall(448, 192 + k * 32, "room/imgs/Walls/MurVe.png"));
				this.addDeco(new Wall(320 + k * 32, 192, "room/imgs/Walls/MurTop2.png"));
			}
			this.addDeco(new Wall(288, 192 + 0 * 32, "room/imgs/Walls/MurVeH.png"));
			this.addDeco(new Wall(448, 192 + 0 * 32, "room/imgs/Walls/MurVeH.png"));
			this.addDeco(new Wall(288, 192 + 4 * 32, "room/imgs/Walls/MurTop2.png"));
			this.addDeco(new Wall(448, 192 + 4 * 32, "room/imgs/Walls/MurTop2.png"));
		}
		else if (randomWallPattern <= 0.500000) { // 4 murs rectangulaires au centre, chemin en forme de +
			for (int i = 0; i < 14; i++) {
				for (int j = 0; j < 8; j++) {
					if (i != 6 && i != 7 && j != 3 && j != 4) {
						if(j == 2 || j == 7)
							this.addDeco(new Wall(160 + i * 32, 128 + j * 32, "room/imgs/Walls/MurTop2.png"));
						else if((j == 0 || j == 5) && (i == 0 || i == 8))
							this.addDeco(new Wall(160 + i * 32, 128 + j * 32, "room/imgs/Walls/MurGH.png"));
						else if((j == 0 || j == 5) && (i == 5 || i == 13))
							this.addDeco(new Wall(160 + i * 32, 128 + j * 32, "room/imgs/Walls/MurDH.png"));
						else if(j == 0 || j == 5)
							this.addDeco(new Wall(160 + i * 32, 128 + j * 32, "room/imgs/Walls/MurB.png"));
						else if(i == 5 || i == 13)
							this.addDeco(new Wall(160 + i * 32, 128 + j * 32, "room/imgs/Walls/MurG.png"));
						else if(i == 0 || i == 8)
							this.addDeco(new Wall(160 + i * 32, 128 + j * 32, "room/imgs/Walls/MurD.png"));
						else
							this.addDeco(new Wall(160 + i * 32, 128 + j * 32, "room/imgs/Walls/BlockNoir.png"));
					}
				}
			}
		}
		else if (randomWallPattern <= 0.666666) { // Ligne de murs des 2 côtés de la salle
			for (int k = 0; k < 12; k++) {
				if (k != 5 && k != 6) {
					if(k == 0 || k == 7) {
						this.addDeco(new Wall(192, 64 + k * 32, "room/imgs/Walls/MurVeH.png"));
						this.addDeco(new Wall(544, 64 + k * 32, "room/imgs/Walls/MurVeH.png"));
					}
					else if(k == 4 || k == 11) {
						this.addDeco(new Wall(192, 64 + k * 32, "room/imgs/Walls/MurVeB.png"));
						this.addDeco(new Wall(544, 64 + k * 32, "room/imgs/Walls/MurVeB.png"));
					}
					else {
						this.addDeco(new Wall(192, 64 + k * 32, "room/imgs/Walls/MurVe.png"));
						this.addDeco(new Wall(544, 64 + k * 32, "room/imgs/Walls/MurVe.png"));
					}
				}
			}
		}
		else if (randomWallPattern <= 0.833333) { // Murs en forme de [ ] au centre
			for (int k = 0; k < 8; k++) {
				if(k == 0) {
					this.addDeco(new Wall(192, 128+ k * 32, "room/imgs/Walls/MurVeH.png"));
					this.addDeco(new Wall(544, 128 + k * 32, "room/imgs/Walls/MurVeH.png"));
				}
				else if(k == 7) {
					this.addDeco(new Wall(192, 128 + k * 32, "room/imgs/Walls/MurVeB.png"));
					this.addDeco(new Wall(544, 128 + k * 32, "room/imgs/Walls/MurVeB.png"));
				}
				else {
					this.addDeco(new Wall(192, 128 + k * 32, "room/imgs/Walls/MurVe.png"));
					this.addDeco(new Wall(544, 128 + k * 32, "room/imgs/Walls/MurVe.png"));
				}
			}
			for (int l = 0; l < 10; l++) {
				if (l < 3 || l > 6) {
					this.addDeco(new Wall(224 + l * 32, 128, "room/imgs/Walls/MurTop2.png"));
					this.addDeco(new Wall(224 + l * 32, 352, "room/imgs/Walls/MurTop2.png"));
				}
			}
		}
	}

	/* Partie Ennemis */

	/* Fonction servant à obtenir des coordonnées valides afin d'empêcher de placer une potion, un coffre ou un ennemi dans un mur
	 * On génère des coordonnées aléatoires jusqu'à ce qu'elles soient éloignées de chaque Item (hors sol) d'au moins 32 (les sprites faisant 32x32).
	 */
	public double[] getValidCoordinates() {
		double x = 0;
		double y = 0;
		boolean valid;
		do {
			x = Math.random() * 608 + 80;
			y = Math.random() * 352 + 80;
			valid = true;
			for(int i = 0; i < this.decoList.size(); i++)
				if(!(this.decoList.get(i) instanceof Tile) && Math.sqrt(Math.pow(x - this.decoList.get(i).getX(), 2) + Math.pow(y - this.decoList.get(i).getY(), 2)) <= 32)
					valid = false;
		} while(!valid);

		return new double[] {x, y};
	}

	public void ajouterEnnemis() {
		double[] coord;

		// On ajoute autant d'ennemis que le niveau de difficulté + 1
		for(int nb = 0; nb < this.difficulty + 1; nb++) {
			coord = getValidCoordinates();
			Fantassin f = new Fantassin(coord[0], coord[1], "DOWN");
			this.addEnnemis(f);
		}

		// Si la salle est difficile, on ajoute un Chef aux trois Fantassins présents
		if(this.difficulty == 2) {
			coord = getValidCoordinates();
			Chef c = new Chef(coord[0], coord[1], "DOWN");
			this.addEnnemis(c);
		}
	}

	/* Partie Objets */

	/* Fonction similaire servant à placer les objets du coffre dans son périmètre ET à un endroit valide.
	 * On fait de même que dans la fonction précédente, mais en prenant pour origine la position du coffre.
	 * On augmente également petit à petit le rayon de recherche pour être sûr de trouver une place valide.
	 */
	public double[] getValidCoordinatesForChestItems(double chest_x, double chest_y) {
		double x = 0;
		double y = 0;
		double radius = 32;
		boolean valid;
		do {
			x = chest_x + (Math.random() < 0.5 ? 1 : -1) * Math.random() * radius;
			y = chest_y + (Math.random() < 0.5 ? 1 : -1) * Math.random() * radius;
			valid = true;
			for(int i = 0; i < this.decoList.size(); i++)
				if(!(this.decoList.get(i) instanceof Tile) && Math.sqrt(Math.pow(x - this.decoList.get(i).getX(), 2) + Math.pow(y - this.decoList.get(i).getY(), 2)) <= 32 || Math.sqrt(Math.pow(x - chest_x, 2) + Math.pow(y - chest_y, 2)) <= 40)
					valid = false;

			radius++;
		} while(!valid);

		return new double[] {x, y};
	}

	//Coffre à 15% de chances
	private int getCoffre15(int healthPotionNumber){
		double[] coord = getValidCoordinates();
		Coffre chest = new Coffre(coord[0], coord[1]);

		// Le coffre contient entre une et toutes les potions de la salle
		int potionNumberInChest = (int)(1 + Math.random() * healthPotionNumber);

		for(int i = 0; i < potionNumberInChest; i++) {
			coord = getValidCoordinatesForChestItems(chest.getX(), chest.getY());
			PotionVie pot = new PotionVie(coord[0], coord[1]);
			chest.addItem(pot);
		}

		//Ajout du coffre dans la salle et retrait des potions du coffre au nombre de potions dans la salle
		this.addObjet(chest);
		return potionNumberInChest;
	}

	//Coffre à 30% de chance
	private int getCoffre30(int attackPotionNumber){
		double[] coord = getValidCoordinates();
		Coffre chest = new Coffre(coord[0], coord[1]);

		// Le coffre contient entre une et toutes les potions de la salle
		int potionNumberInChest = (int)(1 + Math.random() * attackPotionNumber);

		for(int i = 0; i < potionNumberInChest; i++) {
			coord = getValidCoordinatesForChestItems(chest.getX(), chest.getY());
			PotionAtk pot = new PotionAtk(coord[0], coord[1]);
			chest.addItem(pot);
		}

		//Ajout du coffre dans la salle et retrait des potions du coffre au nombre de potions dans la salle
		this.addObjet(chest);
		return potionNumberInChest;
	}
	

	public void addItems() {
		double[] coord;
		/* Facile : 1 potion
		 * Moyen : 1 à 2 potions
		 * Difficile : 1 à 3 potions
		 */
		int healthPotionNumber = (int) (1 + Math.random() * (this.difficulty + 1));

		// Si la salle contient au moins une potion, on génère un coffre à 15% de chance
		if(Math.random() <= 0.15 && healthPotionNumber >= 1) {
			int potionNumberInChest = getCoffre15(healthPotionNumber);			
			healthPotionNumber -= potionNumberInChest;
		}
		// Placement des potions restantes, s'il en reste
		for (int nb = 0; nb < healthPotionNumber; nb++) {
			coord = getValidCoordinates();
			PotionVie pot = new PotionVie(coord[0], coord[1]);
			this.addObjet(pot);
		}

		/* Facile : 0 potion
		 * Moyen : 0 à 1 potions
		 * Difficile : 0 à 2 potions
		 */
		int attackPotionNumber = (int) (0.5 + Math.random() * this.difficulty);
		

		// Si la salle contient au moins une potion, on génère un coffre à 30% de chance
		if(Math.random() <= 0.30 && attackPotionNumber >= 1) {
			int potionNumberInChest = getCoffre30(attackPotionNumber);
			attackPotionNumber -= potionNumberInChest;
		}
		// Placement des potions restantes, s'il en reste
		for (int nb = 0; nb < attackPotionNumber; nb++) {
			coord = getValidCoordinates();
			PotionAtk pot = new PotionAtk(coord[0], coord[1]);
			this.addObjet(pot);
		}
	}

	/* Fonction servant à placer les portes menant à la salle suivante.
	 * Paramètre : la liste de toutes les salles afin d'indiquer la bonne destination
	 * Paramètre : chaine de 4 caractères servant à coder la présente d'une porte ou non (exemple : "H0B0" pour "Haut" et "Bas")
	 */
	public void addDoors(Salle[][] liste_salle, char[] doorLocation) {
		if(doorLocation[0] == 'H') {
			Porte p = new Porte(370, 76, liste_salle[this.x][(this.y)-1]);
			this.addObjet(p);
		}
		if(doorLocation[1] == 'D') {
			Porte p = new Porte(654, 242, liste_salle[(this.x)+1][this.y]);
			this.addObjet(p);
		}
		if(doorLocation[2] == 'B') {
			Porte p = new Porte(370, 408, liste_salle[this.x][(this.y)+1]);
			this.addObjet(p);
		}
		if(doorLocation[3] == 'G') {
			Porte p = new Porte(72, 248, liste_salle[(this.x)-1][this.y]);
			this.addObjet(p);
		}
	}



	// A implémenter avec les observables
	// public void useBonus(){
	// 	System.out.println("Bonus activé");
	// 	for (int k = 0; k < this.subscribers.size(); k++) {
	// 		Item it = this.subscribers.get(k);
	// 		System.out.println("Avant bonus : "+it);
	// 		it.update();
	// 		System.out.println("Après bonus : "+it);
	// 	}
	// }



}