package room;

public class Point {

    private int x;
    private int y;

    public Point(int arg_x, int arg_y) {
        this.x = arg_x;
        this.y = arg_y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    /*public double distance(Point arg) {
        return Math.sqrt(Math.pow(arg.getX() - this.x, 2) + Math.pow(arg.getY() - this.y, 2));
    }*/
}