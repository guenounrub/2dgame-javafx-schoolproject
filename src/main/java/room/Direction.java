package room;

public enum Direction {
    HAUT(0,"UP"),
    DROITE(1,"RIGHT"),
    BAS(2,"DOWN"),
    GAUCHE(3,"LEFT");

    public int value;
    public String nom;

    Direction(int v,String nom) {
        this.value = v;
        this.nom = nom;
    }


}